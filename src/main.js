var colors = {
  white: [255, 255, 255],
  black: [0, 0, 0],
  a: [124, 0, 255],
  t: [215, 255, 0],
  g: [0, 216, 0],
  c: [231, 0, 0],
  orange: [230, 81, 0]
}

var states = {
  title: true,
  intro: false,
  gif: false,
  picture: false,
  progress: -1,
  next: 0,
  list: false,
  convert: false,
  count: 0,
  nucleotides: false,
  synth: false,
  outro: false,
  fadeOut: false,
  frames: 0
}

var texts = {
  font: [],
  title: ['HDD', 'DNA'],
  intro: ["DNA as a storage medium has enormous potential because of its high storage density. DNA — which consists of long chains of the nucleotides A, T, C and G — is life’s information-storage material. Data can be stored in the sequence of these letters, turning DNA into a new form of information technology. DNA is also incredibly stable, as has been demonstrated by the complete genome sequencing of a fossil horse that lived more than 500,000 years ago. And storing it does not require much energy. But it is the storage capacity that shines. DNA can accurately stow massive amounts of data at a density far exceeding that of electronic devices. The simple bacterium Escherichia coli, for instance, has a storage density of about 10^19 bits per cubic centimeter.", "So why not store a little GIF into DNA?"],
  gif: "What about the animated galloping horse of Muybridge?",
  picture: ["Let's take the very first frame of the GIF.", "Then take the pixels of the picture one by one in a given order — from left to right, from top to bottom — and in this particular case the grayscale value of them.", "And then take the next one.", "And the next one.", "And so on..."],
  next: "Then do the same with every single frame.",
  list: ["Now we have an enormous list of RGBs, fortunately they are grayscale RGB codes, so each of them can be simplified to a single number between 0 and 255.", "Let's convert them into binary."],
  nucleotides: "Now that it is finished, eight digit binaries can be converted into nucleotide pairs — zero means an adenine-thymine pair, one means a guanine-cytosine pair —, and then synthetized into a real DNA sequence.",
  outro: ["After synthesizing the DNA sequence, it can be injected into a bacteria in a viral form, so the bacteria's immunological defense mechanism will insert the artificial DNA snippet into its own DNA. Storing it for good.", "Voilà!"]
}

var board = {
  x: null,
  y: null
}

var keys = {
  space: 32,
  enter: 13
}

var images = {
  gif: []
}

var list = {
  dec: [],
  bin: [],
  nuc: []
}

var nucl = [
  ["AT", "TA"],
  ["GC", "CG"]
]

function preload() {
  texts.font.push(loadFont('ttf/Montserrat-Black.ttf'))
  texts.font.push(loadFont('ttf/Roboto-Bold.ttf'))
  texts.font.push(loadFont('ttf/SourceCodePro-Bold.ttf'))
  for (var i = 0; i < 16; i++) {
    images.gif.push(loadImage('img/frame' + (i + 1) + '.png'))
  }
}

function setup() {
  if (windowWidth >= windowHeight * 2) {
    board.y = windowHeight * 0.95
    board.x = board.y * 2
  } else {
    board.x = windowWidth * 0.95
    board.y = board.x * (1 / 2)
  }
  createCanvas(windowWidth, windowHeight)
  for (var i = 0; i < images.gif.length; i++) {
    for (var j = 0; j < 42; j++) {
      for (var k = 0; k < 32; k++) {
        list.dec.push(images.gif[i].get(20 * j, 20 * k)[0])
        list.bin.push(Number(images.gif[i].get(20 * j, 20 * k)[0]).toString(2))
      }
    }
  }
  for (var i = 0; i < 5; i++) {
    list.dec.unshift('')
    list.bin.unshift('')
  }
  for (var i = 0; i < list.bin.length; i++) {
    if (list.bin[i].length < 8) {
      if (i > 5) {
        temp = list.bin[i].length
        for (var j = 0; j < (8 - temp); j++) {
          list.bin[i] = "0" + list.bin[i]
        }
      }
    }
  }
  for (var i = 5; i < list.bin.length; i++) {
    for (var j = 0; j < list.bin[i].length; j++) {
      if (list.bin[i][j] == 0) {
        list.nuc.push(nucl[0][Math.floor(Math.random() * 2)])
      } else {
        list.nuc.push(nucl[1][Math.floor(Math.random() * 2)])
      }
    }
  }
}

function draw() {
  background(colors.white)
  colorMode(RGB, 255, 255, 255, 1)
  rectMode(CENTER)
  textAlign(CENTER, CENTER)
  textFont(texts.font[0])

  if (states.title === true) {
    if (states.fadeOut === false) {
      fill(colors.black)
      textSize(board.x * 0.2)
      noStroke()
      text(texts.title[0], windowWidth * 0.5 - board.x * 0.175, windowHeight * 0.5 - board.y * 0.1)
      fill(colors.orange)
      noStroke()
      text(texts.title[1], windowWidth * 0.5 + board.x * 0.175, windowHeight * 0.5 - board.y * 0.05)
    } else {
      states.frames += deltaTime * 0.001
      fill(colors.black[0], colors.black[1], colors.black[2], 1 - states.frames)
      textSize(board.x * 0.2)
      noStroke()
      text(texts.title[0], windowWidth * 0.5 - board.x * 0.175 - board.x * states.frames * 0.2, windowHeight * 0.5 - board.y * 0.1)
      fill(colors.orange[0], colors.orange[1], colors.orange[2], 1 - states.frames)
      noStroke()
      text(texts.title[1], windowWidth * 0.5 + board.x * 0.175 + board.x * states.frames * 0.2, windowHeight * 0.5 - board.y * 0.05)
    }
  }
  if (states.intro === true) {
    textAlign(LEFT, CENTER)
    textFont(texts.font[1])
    textSize(board.x * 0.025)
    if (states.fadeOut === false) {
      fill(colors.black)
    } else {
      states.frames += deltaTime * 0.001
      fill(colors.black[0], colors.black[1], colors.black[2], 1 - states.frames)
    }
    noStroke()
    text(texts.intro[0], windowWidth * 0.5, windowHeight * 0.5 - board.y * 0.05, board.x * 0.8, board.y * 0.8)
    if (states.fadeOut === false) {
      fill(colors.orange)
    } else {
      states.frames += deltaTime * 0.001
      fill(colors.orange[0], colors.orange[1], colors.orange[2], 1 - states.frames)
    }
    text(texts.intro[1], windowWidth * 0.5, windowHeight * 0.5 + board.y * 0.375, board.x * 0.8, board.y * 0.8)
  }
  if (states.gif === true) {
    fill(colors.black)
    noStroke()
    rect(windowWidth * 0.5 - board.x * 0.25, windowHeight * 0.5 - board.x * 0.05, board.x * 0.44, board.x * 0.34)
    image(images.gif[Math.floor((frameCount * 0.2) % 16)], windowWidth * 0.5 - board.x * 0.21 - board.x * 0.25, windowHeight * 0.5 - board.x * 0.16 - board.x * 0.05, board.x * 0.42, board.x * 0.32)
    textAlign(LEFT, CENTER)
    textFont(texts.font[1])
    textSize(board.x * 0.025)
    if (states.fadeOut === false) {
      fill(colors.black)
    } else {
      states.frames += deltaTime * 0.001
      fill(colors.black[0], colors.black[1], colors.black[2], 1 - states.frames)
    }
    noStroke()
    text(texts.gif, windowWidth * 0.5 - board.x * 0.47, windowHeight * 0.5 + board.y * 0.325 - board.x * 0.025)
  }
  if (states.picture === true) {
    fill(colors.black)
    noStroke()
    rect(windowWidth * 0.5 - board.x * 0.25, windowHeight * 0.5 - board.x * 0.05, board.x * 0.44, board.x * 0.34)
    image(images.gif[states.next], windowWidth * 0.5 - board.x * 0.21 - board.x * 0.25, windowHeight * 0.5 - board.x * 0.16 - board.x * 0.05, board.x * 0.42, board.x * 0.32)
    textAlign(LEFT, TOP)
    textFont(texts.font[1])
    textSize(board.x * 0.025)
    if (states.next === 0) {
      if (states.progress === -1) {
        if (states.fadeOut === false) {
          fill(colors.black)
        } else {
          states.frames += deltaTime * 0.001
          fill(colors.black[0], colors.black[1], colors.black[2], 1 - states.frames)
        }
        noStroke()
        text(texts.picture[0], windowWidth * 0.5 - board.x * 0.07, windowHeight * 0.5 + board.y * 0.7 - board.x * 0.025, board.x * 0.8, board.y * 0.8)
      } else {
        if (states.progress > 3 && states.progress < 1346) {
          states.progress += deltaTime * 0.05
        }
        if (states.progress > 1345) {
          states.progress = 0
          states.next++
        }
        for (var i = 0; i < 42; i++) {
          for (var j = 0; j < 32; j++) {
            if (Math.floor(states.progress) === j * 42 + i) {
              if (states.progress < texts.picture.length) {
                if (states.fadeOut === false) {
                  fill(colors.black)
                } else {
                  states.frames += deltaTime * 0.001
                  fill(colors.black[0], colors.black[1], colors.black[2], 1 - states.frames)
                }
                noStroke()
                text(texts.picture[j * 42 + i + 1], windowWidth * 0.5 - board.x * 0.07, windowHeight * 0.5 + board.y * 0.7 - board.x * 0.025, board.x * 0.8, board.y * 0.8)
              }

              noFill()
              stroke(colors.orange)
              strokeWeight(2)
              rect(windowWidth * 0.5 - board.x * 0.215 - board.x * 0.01 * 24 + i * board.x * 0.01, windowHeight * 0.5 - board.x * 0.165 - board.x * 0.01 * 4 + j * board.x * 0.01, board.x * 0.01, board.x * 0.01)
              line(windowWidth * 0.5 - board.x * 0.21 - board.x * 0.01 * 24 + i * board.x * 0.01, windowHeight * 0.5 - board.x * 0.17 - board.x * 0.01 * 4 + j * board.x * 0.01, windowWidth * 0.5 + board.x * 0.08, windowHeight * 0.5 - board.x * 0.218)
              line(windowWidth * 0.5 - board.x * 0.21 - board.x * 0.01 * 24 + i * board.x * 0.01, windowHeight * 0.5 - board.x * 0.16 - board.x * 0.01 * 4 + j * board.x * 0.01, windowWidth * 0.5 + board.x * 0.08, windowHeight * 0.5 + board.x * 0.118)

              fill(colors.orange)
              noStroke()
              rect(windowWidth * 0.5 + board.x * 0.25, windowHeight * 0.5 - board.x * 0.05, board.x * 0.34, board.x * 0.34)
              fill(images.gif[0].get(20 * i, 20 * j))
              rect(windowWidth * 0.5 + board.x * 0.25, windowHeight * 0.5 - board.x * 0.05, board.x * 0.32, board.x * 0.32)

              fill(colors.orange)
              noStroke()
              textFont(texts.font[0])
              textAlign(CENTER, CENTER)
              textSize(board.x * 0.03)
              text("rgb(" + images.gif[0].get(20 * i, 20 * j)[0] + ", " + images.gif[0].get(20 * i, 20 * j)[0] + ", " + images.gif[0].get(20 * i, 20 * j)[0] + ")", windowWidth * 0.5 + board.x * 0.25, windowHeight * 0.5 - board.x * 0.05)
            }
          }
        }
      }
    } else if (states.next > 0 && states.next < 16) {
      if (states.progress < 1346) {
        states.progress += deltaTime * 0.05 * (states.next + 1)
      }
      if (states.progress > 1345 && states.next < 15) {
        states.progress = 0
        states.next++
      }
      if (states.next === 15) {
        states.picture = false
        states.list = true
      }
      textAlign(LEFT, TOP)
      textFont(texts.font[1])
      textSize(board.x * 0.025)
      noStroke()
      text(texts.next, windowWidth * 0.5 - board.x * 0.07, windowHeight * 0.5 + board.y * 0.7 - board.x * 0.025, board.x * 0.8, board.y * 0.8)
      for (var i = 0; i < 42; i++) {
        for (var j = 0; j < 32; j++) {
          if (Math.floor(states.progress) === j * 42 + i) {
            noFill()
            stroke(colors.orange)
            strokeWeight(2)
            rect(windowWidth * 0.5 - board.x * 0.215 - board.x * 0.01 * 24 + i * board.x * 0.01, windowHeight * 0.5 - board.x * 0.165 - board.x * 0.01 * 4 + j * board.x * 0.01, board.x * 0.01, board.x * 0.01)
            line(windowWidth * 0.5 - board.x * 0.21 - board.x * 0.01 * 24 + i * board.x * 0.01, windowHeight * 0.5 - board.x * 0.17 - board.x * 0.01 * 4 + j * board.x * 0.01, windowWidth * 0.5 + board.x * 0.08, windowHeight * 0.5 - board.x * 0.218)
            line(windowWidth * 0.5 - board.x * 0.21 - board.x * 0.01 * 24 + i * board.x * 0.01, windowHeight * 0.5 - board.x * 0.16 - board.x * 0.01 * 4 + j * board.x * 0.01, windowWidth * 0.5 + board.x * 0.08, windowHeight * 0.5 + board.x * 0.118)

            fill(colors.orange)
            noStroke()
            rect(windowWidth * 0.5 + board.x * 0.25, windowHeight * 0.5 - board.x * 0.05, board.x * 0.34, board.x * 0.34)
            fill(images.gif[0].get(20 * i, 20 * j))
            rect(windowWidth * 0.5 + board.x * 0.25, windowHeight * 0.5 - board.x * 0.05, board.x * 0.32, board.x * 0.32)

            fill(colors.orange)
            noStroke()
            textFont(texts.font[0])
            textAlign(CENTER, CENTER)
            textSize(board.x * 0.03)
            text("rgb(" + images.gif[0].get(20 * i, 20 * j)[0] + ", " + images.gif[0].get(20 * i, 20 * j)[0] + ", " + images.gif[0].get(20 * i, 20 * j)[0] + ")", windowWidth * 0.5 + board.x * 0.25, windowHeight * 0.5 - board.x * 0.05)
          }
        }
      }
    }
  }
  if (states.list === true) {
    textAlign(LEFT, TOP)
    textFont(texts.font[1])
    textSize(board.x * 0.045)
    noStroke()
    if (states.fadeOut === false) {
      fill(colors.black)
    } else {
      states.frames += deltaTime * 0.001
      fill(colors.black[0], colors.black[1], colors.black[2], 1 - states.frames)
    }
    text(texts.list[0], windowWidth * 0.5, windowHeight * 0.5, board.x * 0.8, board.y * 0.8)
    textAlign(LEFT, TOP)
    if (states.fadeOut === false) {
      fill(colors.orange)
    } else {
      fill(colors.orange[0], colors.orange[1], colors.orange[2], 1 - states.frames)
    }
    text(texts.list[1], windowWidth * 0.5, windowHeight * 0.5 + board.y * 0.7, board.x * 0.8, board.y * 0.8)
  }
  if (states.convert === true) {
    if (states.count < 6) {} else {
      states.count += deltaTime
    }
    if (states.count < 21500) {
      fill(colors.black)
      noStroke()
      textSize(board.x * 0.04)
      textFont(texts.font[2])
      for (var i = 0; i < 11; i++) {
        textAlign(RIGHT, CENTER)
        text(list.dec[i + Math.floor(states.count)], windowWidth * 0.5 - board.x * 0.2 - board.x * 0.05, windowHeight * 0.5 + (i - 5) * board.y * 0.08)
        if (i < 6) {
          text(list.bin[i + Math.floor(states.count)], windowWidth * 0.5 + board.x * 0.4 - board.x * 0.05, windowHeight * 0.5 + (i - 5) * board.y * 0.08)
        }
      }
      textAlign(CENTER, CENTER)
      text(list.bin[5 + Math.floor(states.count)], windowWidth * 0.5 - board.x * 0.05, windowHeight * 0.5)
      text('→', windowWidth * 0.5 - board.x * 0.15 - board.x * 0.05, windowHeight * 0.5)
      text('→', windowWidth * 0.5 + board.x * 0.15 - board.x * 0.05, windowHeight * 0.5)
    } else {
      states.convert = false
      states.nucleotides = true
    }
  }
  if (states.nucleotides === true) {
    textAlign(LEFT, TOP)
    textFont(texts.font[1])
    textSize(board.x * 0.045)
    noStroke()
    if (states.fadeOut === false) {
      fill(colors.black)
    } else {
      states.frames += deltaTime * 0.001
      fill(colors.black[0], colors.black[1], colors.black[2], 1 - states.frames)
    }
    text(texts.nucleotides, windowWidth * 0.5, windowHeight * 0.5 + board.y * 0.05, board.x * 0.8, board.y * 0.8)
  }
  if (states.synth === true) {
    if (states.count > 5) {
      states.count += deltaTime * 0.5
    }
    if (states.count > 21500) {
      states.synth = false
      states.count = 0
      states.outro = true
    }
    fill(colors.black)
    noStroke()
    textFont(texts.font[2])
    textAlign(CENTER, CENTER)
    textSize(board.x * 0.1)
    text(list.bin[5 + Math.floor(states.count)], windowWidth * 0.5, windowHeight * 0.5 - board.y * 0.4)

    fill(colors.black)
    noStroke()
    rect(windowWidth * 0.5, windowHeight * 0.5 - board.y * 0.1, board.x * 0.5, board.y * 0.04)
    rect(windowWidth * 0.5, windowHeight * 0.5 - board.y * 0.1, windowWidth, board.y * 0.01)
    rect(windowWidth * 0.5, windowHeight * 0.5 + board.y * 0.275, board.x * 0.5, board.y * 0.04)
    rect(windowWidth * 0.5, windowHeight * 0.5 + board.y * 0.275, windowWidth, board.y * 0.01)
    for (var j = 0; j < list.bin[5].length; j++) {
      if (list.nuc[8 * Math.floor(states.count) + j][0] === "C") {
        fill(colors.c)
      }
      if (list.nuc[8 * Math.floor(states.count) + j][0] === "G") {
        fill(colors.g)
      }
      if (list.nuc[8 * Math.floor(states.count) + j][0] === "T") {
        fill(colors.t)
      }
      if (list.nuc[8 * Math.floor(states.count) + j][0] === "A") {
        fill(colors.a)
      }
      noStroke()
      rect(windowWidth * 0.5 + (j - 3.5) * board.x * 0.06, windowHeight * 0.5, board.x * 0.03, board.y * 0.15)
      if (list.nuc[8 * Math.floor(states.count) + j][1] === "C") {
        fill(colors.c)
      }
      if (list.nuc[8 * Math.floor(states.count) + j][1] === "G") {
        fill(colors.g)
      }
      if (list.nuc[8 * Math.floor(states.count) + j][1] === "T") {
        fill(colors.t)
      }
      if (list.nuc[8 * Math.floor(states.count) + j][1] === "A") {
        fill(colors.a)
      }
      noStroke()
      rect(windowWidth * 0.5 + (j - 3.5) * board.x * 0.06, windowHeight * 0.5 + board.y * 0.05 + board.y * 0.125, board.x * 0.03, board.y * 0.15)

      fill(colors.black)
      noStroke()
      textSize(board.x * 0.05)
      text(list.nuc[8 * Math.floor(states.count) + j][0], windowWidth * 0.5 + (j - 3.5) * board.x * 0.06, windowHeight * 0.5 - board.y * 0.2)
      text(list.nuc[8 * Math.floor(states.count) + j][1], windowWidth * 0.5 + (j - 3.5) * board.x * 0.06, windowHeight * 0.5 + board.y * 0.05 + board.y * 0.3)

      textSize(board.x * 0.2)
      text('←', windowWidth * 0.5 - board.x * 0.4, windowHeight * 0.5)
      text('←', windowWidth * 0.5 + board.x * 0.4, windowHeight * 0.5)
    }
  }
  if (states.outro === true) {
    fill(colors.black)
    noStroke()
    textFont(texts.font[1])
    if (states.count === 0) {
      textAlign(LEFT, CENTER)
      textSize(board.x * 0.04)
      text(texts.outro[0], windowWidth * 0.5, windowHeight * 0.5, board.x * 0.8, board.y * 0.8)
    }
    if (states.count === 1) {
      textAlign(CENTER, CENTER)
      textSize(board.x * 0.25)
      text(texts.outro[1], windowWidth * 0.5, windowHeight * 0.5 - board.y * 0.05, board.x * 0.8, board.y * 0.8)
    }
    if (states.count === 2) {
      fill(colors.black)
      noStroke()
      rect(windowWidth * 0.5, windowHeight * 0.5, board.x * 0.64, board.x * 0.488)
      image(images.gif[Math.floor((frameCount * 0.25) % 16)], windowWidth * 0.5 - board.x * 0.31, windowHeight * 0.5 - board.x * 0.234, board.x * 0.62, board.x * 0.468)
    }
  }
}

function mousePressed() {
  if (states.title === true) {
    states.fadeOut = true
    setTimeout(function() {
      states.title = false
      states.intro = true
      states.fadeOut = false
      states.frames = 0
    }, 1000)
  }
  if (states.intro === true) {
    states.fadeOut = true
    setTimeout(function() {
      states.intro = false
      states.gif = true
      states.fadeOut = false
      states.frames = 0
    }, 1)
  }
  if (states.gif === true) {
    states.fadeOut = true
    setTimeout(function() {
      states.gif = false
      states.picture = true
      states.fadeOut = false
      states.frames = 0
    }, 1000)
  }
  if (states.picture === true) {
    states.fadeOut = true
    setTimeout(function() {
      states.progress++
      states.fadeOut = false
      states.frames = 0
    }, 1000)
  }
  if (states.list === true) {
    states.fadeOut = true
    setTimeout(function() {
      states.list = false
      states.convert = true
      states.fadeOut = false
      states.frames = 0
    }, 1000)
  }
  if (states.convert === true) {
    states.count++
  }
  if (states.nucleotides === true) {
    states.fadeOut = true
    setTimeout(function() {
      states.nucleotides = false
      states.synth = true
      states.fadeOut = false
      states.frames = 0
      states.count = 0
    }, 1000)
  }
  if (states.synth === true) {
    states.count++
  }
  if (states.outro === true) {
    if (states.count < 2) {
      states.count++
    } else {
      states.title = true
      states.count = 0
      states.outro = false
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight * 2) {
    board.y = windowHeight * 0.95
    board.x = board.y * 2
  } else {
    board.x = windowWidth * 0.95
    board.y = board.x * (1 / 2)
  }
  createCanvas(windowWidth, windowHeight)
}
